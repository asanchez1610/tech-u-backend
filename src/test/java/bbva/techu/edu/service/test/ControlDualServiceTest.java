package bbva.techu.edu.service.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import bbva.techu.edu.exception.ControlDualSinAsignacionesException;
import bbva.techu.edu.model.ArqueoCaja;
import bbva.techu.edu.model.ControlDual;
import bbva.techu.edu.model.Employed;
import bbva.techu.edu.model.Value;
import bbva.techu.edu.repository.ControlDualRepository;
import bbva.techu.edu.service.ArqueoCajaService;
import bbva.techu.edu.service.ControlDualService;
import bbva.techu.edu.service.impl.ControlDualServiceImpl;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class ControlDualServiceTest {	
	
	
	@Mock
    private ArqueoCajaService arqueoCajaService;
	
	@Mock
    private ControlDualRepository controlDualRepository;
	
	@InjectMocks
	private ControlDualService service = new ControlDualServiceImpl();

	@Test
	public void asignacionDeDosArqueosDebenSupervisarseEntreSi() throws ControlDualSinAsignacionesException {
		
		List<ArqueoCaja> arqueosPendientes = new ArrayList<ArqueoCaja>();
		
		ArqueoCaja arqueoCajaLalo = arqueoCajaLalo();
		arqueosPendientes.add(arqueoCajaLalo);//oficina Chimu
		ArqueoCaja arqueoCajaPepe = arqueoCajaPepe();
		arqueosPendientes.add(arqueoCajaPepe);//oficina Chimu
		
		when(arqueoCajaService.listArqueosPendientesOficina(any(Value.class))).thenReturn(arqueosPendientes);
		when(arqueoCajaService.saveArqueosCaja(any(List.class))).thenAnswer(i -> i.getArguments()[0]);
		when(controlDualRepository.insert(any(ControlDual.class))).thenAnswer(i -> i.getArguments()[0]);
		
		Employed subGerente = subgerenteJose();//oficina Chimu
		
		ControlDual controlDual = service.procesar(subGerente);
		
		assertEquals("OK", controlDual.getEstado());
		assertEquals(2, controlDual.getAgrupaciones().size());
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaLalo));
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaPepe));
		assertEquals(controlDual.getAgrupaciones().get(0).getSolicitante(), controlDual.getAgrupaciones().get(1).getSupervisor());
		assertEquals(controlDual.getAgrupaciones().get(1).getSolicitante(), controlDual.getAgrupaciones().get(0).getSupervisor());
		
		verify(arqueoCajaService).listArqueosPendientesOficina(any(Value.class));
		verify(arqueoCajaService).saveArqueosCaja(any(List.class));
		verify(controlDualRepository).insert(any(ControlDual.class));
		
	}
	
	@Test
	public void asignacionDeUnArqueoDebenSupervisarElAdmin() throws ControlDualSinAsignacionesException {
		
		List<ArqueoCaja> arqueosPendientes = new ArrayList<ArqueoCaja>();
		
		ArqueoCaja arqueoCajaLalo = arqueoCajaLalo();
		arqueosPendientes.add(arqueoCajaLalo);//oficina Chimu
		
		Employed subGerente = subgerenteJose();//oficina Chimu
		
		when(arqueoCajaService.listArqueosPendientesOficina(any(Value.class))).thenReturn(arqueosPendientes);
		when(arqueoCajaService.saveArqueosCaja(any(List.class))).thenAnswer(i -> i.getArguments()[0]);
		when(controlDualRepository.insert(any(ControlDual.class))).thenAnswer(i -> i.getArguments()[0]);
		
		ControlDual controlDual = service.procesar(subGerente);
		
		assertEquals("OK", controlDual.getEstado());
		assertEquals(1, controlDual.getAgrupaciones().size());
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaLalo));
		assertEquals(subGerente, controlDual.getAgrupaciones().get(0).getSupervisor());
		
		verify(arqueoCajaService).listArqueosPendientesOficina(any(Value.class));
		verify(arqueoCajaService).saveArqueosCaja(any(List.class));
		verify(controlDualRepository).insert(any(ControlDual.class));
		
	}
	
	@Test
	public void asignacionDeTresArqueosDebenSupervisarseEntreSi() throws ControlDualSinAsignacionesException {
		
		List<ArqueoCaja> arqueosPendientes = new ArrayList<ArqueoCaja>();
		
		ArqueoCaja arqueoCajaLalo = arqueoCajaLalo();
		arqueosPendientes.add(arqueoCajaLalo);//oficina Chimu
		ArqueoCaja arqueoCajaPepe = arqueoCajaPepe();
		arqueosPendientes.add(arqueoCajaPepe);//oficina Chimu
		ArqueoCaja arqueoCajaLuis = arqueoCajaLuis();
		arqueosPendientes.add(arqueoCajaLuis);//oficina Chimu
		
		Employed subGerente = subgerenteJose();//oficina Chimu
		
		when(arqueoCajaService.listArqueosPendientesOficina(any(Value.class))).thenReturn(arqueosPendientes);
		when(arqueoCajaService.saveArqueosCaja(any(List.class))).thenAnswer(i -> i.getArguments()[0]);
		when(controlDualRepository.insert(any(ControlDual.class))).thenAnswer(i -> i.getArguments()[0]);
		
		ControlDual controlDual = service.procesar(subGerente);
		
		assertEquals("OK", controlDual.getEstado());
		assertEquals(3, controlDual.getAgrupaciones().size());
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaLalo));
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaPepe));
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaLuis));
		assertNotNull(controlDual.getAgrupaciones().get(0).getSupervisor());
		assertNotNull(controlDual.getAgrupaciones().get(1).getSupervisor());
		assertNotNull(controlDual.getAgrupaciones().get(2).getSupervisor());

		verify(arqueoCajaService).listArqueosPendientesOficina(any(Value.class));
		verify(arqueoCajaService).saveArqueosCaja(any(List.class));
		verify(controlDualRepository).insert(any(ControlDual.class));
		
	}
	
	
	@Test
	public void asignacionDeCuatroArqueosDebenSupervisarseEntreSi() throws ControlDualSinAsignacionesException {
		
		List<ArqueoCaja> arqueosPendientes = new ArrayList<ArqueoCaja>();
		
		ArqueoCaja arqueoCajaLalo = arqueoCajaLalo();
		arqueosPendientes.add(arqueoCajaLalo);//oficina Chimu
		ArqueoCaja arqueoCajaPepe = arqueoCajaPepe();
		arqueosPendientes.add(arqueoCajaPepe);//oficina Chimu
		ArqueoCaja arqueoCajaLuis = arqueoCajaLuis();
		arqueosPendientes.add(arqueoCajaLuis);//oficina Chimu
		ArqueoCaja arqueoCajaFlor = arqueoCajaFlor();
		arqueosPendientes.add(arqueoCajaFlor);//oficina Chimu
		
		Employed subGerente = subgerenteJose();//oficina Chimu
		
		when(arqueoCajaService.listArqueosPendientesOficina(any(Value.class))).thenReturn(arqueosPendientes);
		when(arqueoCajaService.saveArqueosCaja(any(List.class))).thenAnswer(i -> i.getArguments()[0]);
		when(controlDualRepository.insert(any(ControlDual.class))).thenAnswer(i -> i.getArguments()[0]);
		
		ControlDual controlDual = service.procesar(subGerente);
		
		assertEquals("OK", controlDual.getEstado());
		assertEquals(4, controlDual.getAgrupaciones().size());
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaLalo));
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaPepe));
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaLuis));
		assertTrue(controlDual.getAgrupaciones().contains(arqueoCajaFlor));
		assertNotNull(controlDual.getAgrupaciones().get(0).getSupervisor());
		assertNotNull(controlDual.getAgrupaciones().get(1).getSupervisor());
		assertNotNull(controlDual.getAgrupaciones().get(2).getSupervisor());
		assertNotNull(controlDual.getAgrupaciones().get(3).getSupervisor());

		verify(arqueoCajaService).listArqueosPendientesOficina(any(Value.class));
		verify(arqueoCajaService).saveArqueosCaja(any(List.class));
		verify(controlDualRepository).insert(any(ControlDual.class));
		
	}

	private ArqueoCaja arqueoCajaFlor() {
		ArqueoCaja arqueoCaja = new ArqueoCaja();
		arqueoCaja.setFechaSolicitud(new Date());
		arqueoCaja.setEstado("Registrado");
		arqueoCaja.setNroCaja("10");
		arqueoCaja.setSolicitante(new Employed());
		arqueoCaja.getSolicitante().setNombres("Flor");
		arqueoCaja.setOficina(oficinaChimu());
		return arqueoCaja;
	}

	private ArqueoCaja arqueoCajaLuis() {
		ArqueoCaja arqueoCaja = new ArqueoCaja();
		arqueoCaja.setFechaSolicitud(new Date());
		arqueoCaja.setEstado("Registrado");
		arqueoCaja.setNroCaja("5");
		arqueoCaja.setSolicitante(new Employed());
		arqueoCaja.getSolicitante().setNombres("Luis");
		arqueoCaja.setOficina(oficinaChimu());
		return arqueoCaja;
	}

	private Employed subgerenteJose() {
		Employed jose = new Employed();
		jose.setOficina(oficinaChimu());
		jose.setRol(new Value());
		jose.getRol().setCode("admin");
		jose.getRol().setName("Administrador");
		return jose;
	}

	private ArqueoCaja arqueoCajaLulu() {
		ArqueoCaja arqueoCaja = new ArqueoCaja();
		arqueoCaja.setFechaSolicitud(new Date());
		arqueoCaja.setEstado("Registrado");
		arqueoCaja.setNroCaja("1");
		arqueoCaja.setSolicitante(new Employed());
		arqueoCaja.getSolicitante().setNombres("Lulu");
		arqueoCaja.setOficina(oficinaArequipa());
		return arqueoCaja;
	}

	private Value oficinaArequipa() {
		Value oficinaChimu = new Value();
		oficinaChimu.setCode("0002");
		oficinaChimu.setName("Oficina Arequipa");
		return oficinaChimu;
	}

	private ArqueoCaja arqueoCajaPepe() {
		ArqueoCaja arqueoCaja = new ArqueoCaja();
		arqueoCaja.setFechaSolicitud(new Date());
		arqueoCaja.setEstado("Registrado");
		arqueoCaja.setNroCaja("1");
		arqueoCaja.setSolicitante(new Employed());
		arqueoCaja.getSolicitante().setNombres("Pepe");
		arqueoCaja.setOficina(oficinaChimu());
		return arqueoCaja;
	}

	private Value oficinaChimu() {
		Value oficinaChimu = new Value();
		oficinaChimu.setCode("0001");
		oficinaChimu.setName("Oficina Chimu");
		return oficinaChimu;
	}

	private ArqueoCaja arqueoCajaLalo() {
		ArqueoCaja arqueoCaja = new ArqueoCaja();
		arqueoCaja.setFechaSolicitud(new Date());
		arqueoCaja.setEstado("Registrado");
		arqueoCaja.setNroCaja("2");
		arqueoCaja.setSolicitante(new Employed());
		arqueoCaja.getSolicitante().setNombres("Lalo");
		arqueoCaja.setOficina(oficinaChimu());
		return arqueoCaja;
	}
	
}
