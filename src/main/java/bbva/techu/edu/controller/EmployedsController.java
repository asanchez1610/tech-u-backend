package bbva.techu.edu.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bbva.techu.edu.model.Employed;
import bbva.techu.edu.security.utils.JWTUtil;
import bbva.techu.edu.service.EmployedService;
import bbva.techu.edu.utils.Constants;
import bbva.techu.edu.utils.NegocioException;
import bbva.techu.edu.utils.ResponseError;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api")
@Tag(name = "EmployedsController", description = "Endponts del modelo de empleados")
public class EmployedsController {

	@Autowired
	EmployedService employedService;

	@GetMapping("/employeds")
	public ResponseEntity<List<Employed>> findEmployed(@RequestHeader(Constants.Security.HEADER) String tokenBearer) {
		Employed userSession = JWTUtil.getUserSession(tokenBearer);
		System.out.println(userSession);
		List<Employed> groups = this.employedService.findEmployedsAll();
		return new ResponseEntity<>(groups, HttpStatus.OK);
	}

	@PostMapping("/employeds")
	public ResponseEntity<?> saveEmployed(@RequestBody Employed employed) {
		try {
			if (employed.getId() != null) {
				this.employedService.saveEmployed(employed);
				return new ResponseEntity<>(employed, HttpStatus.OK);
			}
			employed.setState(Constants.States.ACTIVE);
			employed.setCreateAt(new Date());
			this.employedService.saveEmployed(employed);
			return new ResponseEntity<>(employed, HttpStatus.CREATED);
		} catch (NegocioException e) {
			return new ResponseEntity<ResponseError>(new ResponseError(e.getMessage()), e.getCodeError());
		}

	}

	@DeleteMapping("/employeds/{id}")
	public ResponseEntity<?> removeEmployed(@PathVariable String id) {
		Employed employed = new Employed();
		employed.setId(id);
		this.employedService.removeEmployed(employed);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/employeds/{id}")
	public ResponseEntity<?> getEmployed(@PathVariable String id) {
		Optional<Employed> group = this.employedService.getEmployed(id);
		if (group.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(group, HttpStatus.OK);
	}

}
