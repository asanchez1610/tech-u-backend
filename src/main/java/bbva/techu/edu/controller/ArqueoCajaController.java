package bbva.techu.edu.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bbva.techu.edu.dto.DashboardArqueoCaja;
import bbva.techu.edu.exception.ArqueoCajaExistenPendientesException;
import bbva.techu.edu.exception.ArqueoCajaAsignadoOtroSupervisorException;
import bbva.techu.edu.exception.ArqueoCajaNoEncontradaException;
import bbva.techu.edu.exception.ArqueoCajaNoExisteASupervisarException;
import bbva.techu.edu.exception.ArqueoCajaEstadoErroneoException;
import bbva.techu.edu.model.ArqueoCaja;
import bbva.techu.edu.model.Employed;
import bbva.techu.edu.security.utils.JWTUtil;
import bbva.techu.edu.service.ArqueoCajaService;
import bbva.techu.edu.utils.Constants;
import bbva.techu.edu.utils.ResponseError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api")
@Tag(name = "ArqueoCajaController", description = "Endponts del modelo de arqueo de caja")
public class ArqueoCajaController {

	@Autowired
	private ArqueoCajaService arqueoCajaService;

	@GetMapping("/arqueo-caja")
	@Operation(description = "Listado de Arqueos del usuario y del los supervisados", tags = { "ArqueoCajaController" })
	public ResponseEntity<DashboardArqueoCaja> arqueoCaja(@RequestHeader(Constants.Security.HEADER) String tokenBearer,
			@RequestParam(value = "fechaInicio") String fechaInicio, @RequestParam(value = "fechaFin") String fechaFin)
			throws ParseException {

		Employed empleado = JWTUtil.getUserSession(tokenBearer);

		DashboardArqueoCaja dashboard = new DashboardArqueoCaja();

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

		Date fecIni = dateFormat.parse(fechaInicio + "000000");// 00:00:00
		Date fecFin = dateFormat.parse(fechaFin + "235959");// 23:59:59

		dashboard.setMisArqueos(arqueoCajaService.listMisArqueos(empleado, fecIni, fecFin));
		dashboard.setMisSupervisiones(arqueoCajaService.listMisArqueosSupervisados(empleado, fecIni, fecFin));
		try {
			dashboard.setArqueoASupervisar(arqueoCajaService.getArqueoCajaSupervisar(empleado));
		} catch (ArqueoCajaNoExisteASupervisarException e) {
			
		}

		return ResponseEntity.ok(dashboard);
	}

	@PostMapping("/arqueo-caja")
	public ResponseEntity<?> saveArqueoCaja(@RequestHeader(Constants.Security.HEADER) String tokenBearer,
			@RequestBody ArqueoCaja arqueoCaja) {

		Employed empleado = JWTUtil.getUserSession(tokenBearer);
		if (arqueoCaja.getSaldoSolesBalancin() != null && arqueoCaja.getSaldoDolaresBalancin() != null
				&& arqueoCaja.getSaldoEurosBalancin() != null && arqueoCaja.getSaldoRevisadoSoles() != null
				&& arqueoCaja.getSaldoRevisadoDolares() != null && arqueoCaja.getSaldoRevisadoEuros() != null
				&& arqueoCaja.getNroCaja() != null) {
			try {
				return new ResponseEntity<>(arqueoCajaService.saveArqueoCaja(arqueoCaja, empleado), HttpStatus.OK);
			} catch (ArqueoCajaExistenPendientesException e) {
				return new ResponseEntity<ResponseError>(new ResponseError("El usuario tiene Arqueo de cajas pendientes"),
						HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<ResponseError>(new ResponseError("Ingreso de saldos y caja son obligatorios"),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/arqueo-caja/{id}")
	public ResponseEntity<?> modifyArqueoCaja(@RequestHeader(Constants.Security.HEADER) String tokenBearer,
			@RequestBody ArqueoCaja arqueoCaja, @PathVariable("id") String id) {

		Employed empleado = JWTUtil.getUserSession(tokenBearer);
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

		try {
			return new ResponseEntity<>(arqueoCajaService.modifyArqueoCaja(id, arqueoCaja, empleado), HttpStatus.OK);
		} catch (ArqueoCajaNoEncontradaException e) {
			return new ResponseEntity<ResponseError>(new ResponseError("Arqueo Caja no encontrada"),
					HttpStatus.NOT_FOUND);
		} catch (ArqueoCajaEstadoErroneoException e) {
			return new ResponseEntity<ResponseError>(new ResponseError("Arqueo Caja pendiente de asignación o ya supervisada"),
					HttpStatus.BAD_REQUEST);
		} catch (ArqueoCajaAsignadoOtroSupervisorException e) {
			return new ResponseEntity<ResponseError>(new ResponseError("Arqueo Caja asignado a otro supervisor"),
					HttpStatus.BAD_REQUEST);
		}

	}

	@GetMapping("/arqueo-caja/buscar")
	public ResponseEntity<?> generarConsolidadoOficina(@RequestHeader(Constants.Security.HEADER) String tokenBearer,
													   @RequestParam(value="fechaInicio")
														String fechaInicio,
													   @RequestParam(value="fechaFin")
														String fechaFin) throws ParseException{
		
		Employed empleado = JWTUtil.getUserSession(tokenBearer);
		
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		
		Date fecIni = dateFormat.parse(fechaInicio+"000000");// 00:00:00
		Date fecFin = dateFormat.parse(fechaFin+"235959");// 23:59:59
		return new ResponseEntity<>(arqueoCajaService.generarConsolidadoOficina(fecIni, fecFin, empleado), HttpStatus.OK);
	}
}
