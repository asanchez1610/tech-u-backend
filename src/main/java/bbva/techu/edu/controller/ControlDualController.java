package bbva.techu.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bbva.techu.edu.exception.ControlDualSinAsignacionesException;
import bbva.techu.edu.model.ControlDual;
import bbva.techu.edu.model.Employed;
import bbva.techu.edu.security.utils.JWTUtil;
import bbva.techu.edu.service.ControlDualService;
import bbva.techu.edu.utils.Constants;
import bbva.techu.edu.utils.ResponseError;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api")
@Tag(name = "ControlDualController", description = "Endponts del modelo de control dual")
public class ControlDualController {
	
	@Autowired
	private ControlDualService controlDualService;
	
	@PostMapping("/control-dual")
	public ResponseEntity<?> controlDual(@RequestHeader(Constants.Security.HEADER) String tokenBearer) {
		
		Employed empleado = JWTUtil.getUserSession(tokenBearer);
		
		ControlDual controlDual;
		try {
			controlDual = controlDualService.procesar(empleado);
			 return ResponseEntity.ok(controlDual);
		} catch (ControlDualSinAsignacionesException e) {
			return new ResponseEntity<ResponseError>(new ResponseError("No existen arqueo de cajas para asignar"),
					HttpStatus.NOT_FOUND);
		}
		
		
	}

}
