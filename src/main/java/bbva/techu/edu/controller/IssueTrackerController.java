package bbva.techu.edu.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import bbva.techu.edu.dto.IssueTrackerRequest;
import bbva.techu.edu.dto.IssueTrackerResponse;
import bbva.techu.edu.dto.IssueTrackerTransaction;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/product-issue-tracker/v0")
@Tag(name = "IssueTrackerController", description = "Mocks Issue Tracker")
public class IssueTrackerController {

	@GetMapping("/test")
	@Operation(description = "Test controller Issue Tracker", tags = { "IssueTrackerController" })
	public ResponseEntity<?> test() throws ParseException {
		return ResponseEntity.ok("Mock Issue Tracker Ready!");
	}

	@PostMapping("/issues")
	@Operation(description = "Mock Create Issue Tracker", tags = { "IssueTrackerController" })
	public ResponseEntity<?> createIssueTracker(@RequestBody IssueTrackerRequest issueTracker) throws ParseException {
		IssueTrackerResponse response = this.buildResponseIssueTracker(issueTracker);
		if (response.getTransactions().get(0).getStatus().equals("A")) {
			return ResponseEntity.ok(response);
		} else {
			return ResponseEntity.badRequest().body(this.buildError(response.getTransactions().get(0).getCode(), response.getTransactions().get(0).getMessage()));
		}

	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> buildError(String code, String message) {
		String strJson = "{\r\n" + "\r\n" + "\"messages\": [\r\n" + "\r\n" + "{\r\n" + "\r\n" + "\"code\": \"" + code
				+ "\",\r\n" + "\r\n" + "\"message\": \"" + message + "\",\r\n" + "\r\n" + "\"parameters\": [],\r\n"
				+ "\r\n" + "\"type\": \"FATAL\"\r\n" + "\r\n" + "}\r\n" + "\r\n" + "]\r\n" + "\r\n" + "}";
		Gson gson = new Gson();
		return gson.fromJson(strJson, Map.class);
	}

	private IssueTrackerResponse buildResponseIssueTracker(IssueTrackerRequest issueTracker) {
		IssueTrackerResponse response = new IssueTrackerResponse();
		List<IssueTrackerTransaction> transactions = new ArrayList<>();
		IssueTrackerTransaction transaction = new IssueTrackerTransaction();
		if (issueTracker.getAccountId().equals("00110057740200359721") && issueTracker.getPetitionType().equals("01")) {
			transaction.setStatus("A");
			response.setTicketDate(new Date());
			response.setTicketId("12345678");
		} else if(issueTracker.getAccountId().equals("00110057740200359722") && issueTracker.getPetitionType().equals("01")) {
			transaction.setStatus("R");
			transaction.setCode("BGE0270");
			transaction.setMessage("NO SE PUEDE CANCELAR UNA CUENTA CON RETENCIONES VIGENTES.");
		} else if(issueTracker.getAccountId().equals("00110057740200359723") && issueTracker.getPetitionType().equals("01")) {
			transaction.setStatus("R");
			transaction.setCode("BGE0996");
			transaction.setMessage("NO ES POSIBLE CANCELAR LA CUENTA AL TENER MOVIMIENTOS PENDIENTES.");
		} else if(issueTracker.getAccountId().equals("00110057740200359724") && issueTracker.getPetitionType().equals("02")) {
			response.setTicketDate(new Date());
			response.setTicketId("12345678");
			transaction.setStatus("A");
			transaction.setId("456987021");
			transaction.setMessage("LA CUENTA INFORMADA A SIDO CANCELADA CON EXITO.");
		} else if(!issueTracker.getPetitionType().equals("01") && !issueTracker.getPetitionType().equals("02")) {
			transaction.setStatus("R");
			transaction.setCode("BGE8991");
			transaction.setMessage("TIPO DE PETICION INCORRECTA");
		} else {
			transaction.setStatus("R");
			transaction.setCode("BGE8991");
			transaction.setMessage("LA CUENTA INFORMADA NO EXISTE.");
		}
		transactions.add(transaction);
		response.setTransactions(transactions);
		return response;
	}

}
