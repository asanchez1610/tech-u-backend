package bbva.techu.edu.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import bbva.techu.edu.model.GroupValues;
import bbva.techu.edu.model.Value;
import bbva.techu.edu.service.ValuesService;
import bbva.techu.edu.utils.Constants;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api")
@Tag(name = "ValuesController", description = "Endponts del modelo de valores y grupo de valores")
public class ValuesController {

	@Autowired
	ValuesService valuesService;

	@GetMapping("/groups")
	public ResponseEntity<List<GroupValues>> findGroupValues() {
		List<GroupValues> groups = this.valuesService.findGroupValuesAllActive();
		return new ResponseEntity<>(groups, HttpStatus.OK);
	}

	@PostMapping("/groups")
	public ResponseEntity<?> saveGroupValues(@RequestBody GroupValues group) {
		if (group.getId() != null) {
			this.valuesService.saveGroupValue(group);
			return new ResponseEntity<>(group, HttpStatus.OK);
		}
		group.setState(Constants.States.ACTIVE);
		group.setCreateAt(new Date());
		this.valuesService.saveGroupValue(group);
		return new ResponseEntity<>(group, HttpStatus.CREATED);
	}

	@DeleteMapping("/groups/{id}")
	public ResponseEntity<?> removeGroupValues(@PathVariable String id) {
		GroupValues group = new GroupValues(id, Constants.States.INACTIVE);
		this.valuesService.removeGroupValue(group);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/groups/{id}")
	public ResponseEntity<?> getGroupValues(@PathVariable String id) {
		Optional<GroupValues> group = this.valuesService.getGroupValue(id);
		if (group.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(group, HttpStatus.OK);
	}

	@GetMapping("/{group}/values")
	public ResponseEntity<?> findValuesByGroup(@PathVariable String group) {
		List<Value> values = this.valuesService.findValuesByGroup(group);
		return new ResponseEntity<>(values, HttpStatus.OK);
	}

	@PostMapping("/values")
	public ResponseEntity<?> saveValue(@RequestBody Value value) {
		if (value.getId() != null) {
			this.valuesService.saveValue(value);
			return new ResponseEntity<>(value, HttpStatus.OK);
		}
		value.setState(Constants.States.ACTIVE);
		value.setCreateAt(new Date());
		this.valuesService.saveValue(value);
		return new ResponseEntity<>(value, HttpStatus.CREATED);
	}

	@DeleteMapping("/values/{id}")
	public ResponseEntity<?> removeValue(@PathVariable String id) {
		Value value = new Value();
		value.setId(id);
		this.valuesService.removeValue(value);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/values/{id}")
	public ResponseEntity<?> getValue(@PathVariable String id) {
		Optional<Value> value = this.valuesService.getValue(id);
		if (value.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(value, HttpStatus.OK);
	}

}
