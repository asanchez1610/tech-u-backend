package bbva.techu.edu.security;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import bbva.techu.edu.model.Employed;
import bbva.techu.edu.utils.Constants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenProvider {

	@Value("${app.jwt.time_expired}")
	private int timeExpired;

	public String getJWTToken(String username, Employed user) {
		String secretKey = Constants.Security.SECRET;
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList(Constants.Security.ROL_ANY);
Gson json = new Gson();
		String token = Jwts.builder().setId(Constants.Security.SOFT_TOKEN_ID).setSubject(json.toJson(user))
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + (timeExpired * 1000)))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;
	}

}
