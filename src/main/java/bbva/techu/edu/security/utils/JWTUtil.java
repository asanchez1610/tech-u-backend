package bbva.techu.edu.security.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.google.gson.Gson;

import bbva.techu.edu.model.Employed;

public class JWTUtil {

	private static int entropia = 12;

	public static String hashPassword(String passwordPlainText) {
		String salt = BCrypt.gensalt(entropia);
		String hashedPassword = BCrypt.hashpw(passwordPlainText, salt);
		return (hashedPassword);
	}

	public static boolean checkPassword(String passwordPlainText, String storedHash) {
		boolean passwordVerified = false;
		if (null == storedHash || !storedHash.startsWith("$2a$"))
			throw new java.lang.IllegalArgumentException("Invalido storedHash");
		passwordVerified = BCrypt.checkpw(passwordPlainText, storedHash);
		return (passwordVerified);
	}

	@SuppressWarnings("unchecked")
	public static Employed getUserSession(String tokenBearer) {

		String[] separator = tokenBearer.split(" ");
		String jwtToken = separator[1];

		String[] split_string = jwtToken.split("\\.");
		String base64EncodedBody = split_string[1];

		Base64 base64Url = new Base64(true);

		String body = new String(base64Url.decode(base64EncodedBody));
		Map<String, String> map = new HashMap<>();
		Gson json = new Gson();
		map = json.fromJson(body, Map.class);
		Employed emp = json.fromJson(map.get("sub"), Employed.class);

		return emp;

	}

}
