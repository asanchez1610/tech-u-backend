package bbva.techu.edu.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import bbva.techu.edu.model.Employed;
import bbva.techu.edu.service.SecurityService;
import bbva.techu.edu.utils.AutenticarException;
import bbva.techu.edu.utils.ResponseError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "SecurityController", description = "Endponts de autenticacion y generación de token")
public class SecurityController {

	@Autowired
	private SecurityService securityService;

	@PostMapping("auth")
	@Operation(
			summary = "auth", 
			description = "Autenticacion Generacion de Token",
			tags = { "SecurityController" })
	@io.swagger.v3.oas.annotations.parameters.RequestBody(
			content = @Content(
						mediaType = "application/json", 
						examples = @ExampleObject(value = "{ \n \"registro\": \"employedId\", \n \"password\": \"password\" \n}")))
	public ResponseEntity<?> auth(
			@RequestBody Employed usuario
			) {
		Employed usuarioAuth;
		try {
			usuarioAuth = securityService.auth(usuario);
			if (usuarioAuth != null) {
				return new ResponseEntity<>(usuarioAuth, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (AutenticarException e) {
			return new ResponseEntity<ResponseError>(new ResponseError(e.getMessage()), e.getCodeError());
		}

	}
}
