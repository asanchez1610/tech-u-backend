package bbva.techu.edu.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import bbva.techu.edu.model.ControlDual;

public interface ControlDualRepository extends MongoRepository<ControlDual, String> {
}
