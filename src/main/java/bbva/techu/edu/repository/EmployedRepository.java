package bbva.techu.edu.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import bbva.techu.edu.model.Employed;

public interface EmployedRepository extends MongoRepository<Employed, String> {

	Employed findByRegistroIgnoreCase(String userName);

	List<Employed> findByStateIgnoreCaseAndRegistroNotOrderByCreateAtDesc(String state, String registroAdmin);

}
