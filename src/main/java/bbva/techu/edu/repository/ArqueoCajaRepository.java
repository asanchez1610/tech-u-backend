package bbva.techu.edu.repository;

import java.util.Date;
import java.util.List;

import bbva.techu.edu.model.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import bbva.techu.edu.model.ArqueoCaja;
import bbva.techu.edu.model.Employed;
import bbva.techu.edu.model.Value;

public interface ArqueoCajaRepository extends MongoRepository<ArqueoCaja, String> {

	List<ArqueoCaja> findBySolicitanteAndFechaSolicitudBetween(Employed empleado, Date fechaInicio, Date fechaFin, Sort sort);

	List<ArqueoCaja> findBySupervisorAndFechaSolicitudBetween(Employed empleado, Date fechaInicio, Date fechaFin, Sort sort);

	List<ArqueoCaja> findByOficinaAndFechaSolicitudBetween(Value oficina , Date fechaInicio, Date fechaFin, Sort sort);
	
	List<ArqueoCaja> findByOficinaAndEstadoAndFechaSolicitudBetween(Value oficina, String estado, Date fechaInicio, Date fechaFin);

	boolean existsBySolicitanteAndEstado(Employed empleado, String string);
	
	List<ArqueoCaja> findBySupervisorAndEstado(Employed empleado, String string);

}
