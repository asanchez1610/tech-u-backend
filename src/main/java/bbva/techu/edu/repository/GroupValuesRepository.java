package bbva.techu.edu.repository;

import java.util.List;
import bbva.techu.edu.model.GroupValues;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroupValuesRepository extends MongoRepository<GroupValues, String> {

	List<GroupValues> findByStateIgnoreCaseOrderByCreateAtDesc(String state);

}
