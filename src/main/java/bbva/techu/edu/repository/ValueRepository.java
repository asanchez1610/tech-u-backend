package bbva.techu.edu.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import bbva.techu.edu.model.Value;


public interface ValueRepository extends MongoRepository<Value, String> {

	List<Value> findByGroupCodeIgnoreCaseAndStateIgnoreCaseOrderByCreateAtDesc(String code, String state);
	
}
