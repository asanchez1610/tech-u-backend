package bbva.techu.edu.dto;

import java.util.List;

import bbva.techu.edu.model.ArqueoCaja;

public class DashboardArqueoCaja {
	
	private List<ArqueoCaja> misArqueos;
	private List<ArqueoCaja> misSupervisiones;
	private ArqueoCaja arqueoASupervisar;

	public List<ArqueoCaja> getMisArqueos() {
		return misArqueos;
	}
	public void setMisArqueos(List<ArqueoCaja> misArqueos) {
		this.misArqueos = misArqueos;
	}
	public List<ArqueoCaja> getMisSupervisiones() {
		return misSupervisiones;
	}
	public void setMisSupervisiones(List<ArqueoCaja> misSupervisiones) {
		this.misSupervisiones = misSupervisiones;
	}
	public ArqueoCaja getArqueoASupervisar() {
		return arqueoASupervisar;
	}
	public void setArqueoASupervisar(ArqueoCaja arqueoASupervisar) {
		this.arqueoASupervisar = arqueoASupervisar;
	}

}
