package bbva.techu.edu.dto;

public class IssueTrackerRequest {

	protected String customerId;
	protected String accountId;
	protected String fcrType;
	protected String petitionType;
	protected Boolean isRenounceBalance;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getFcrType() {
		return fcrType;
	}

	public void setFcrType(String fcrType) {
		this.fcrType = fcrType;
	}

	public String getPetitionType() {
		return petitionType;
	}

	public void setPetitionType(String petitionType) {
		this.petitionType = petitionType;
	}

	public Boolean getIsRenounceBalance() {
		return isRenounceBalance;
	}

	public void setIsRenounceBalance(Boolean isRenounceBalance) {
		this.isRenounceBalance = isRenounceBalance;
	}

}
