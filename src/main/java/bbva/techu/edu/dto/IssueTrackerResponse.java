package bbva.techu.edu.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class IssueTrackerResponse {

	@JsonInclude(Include.NON_NULL)
	protected String ticketId;
	@JsonInclude(Include.NON_NULL)
	protected Date ticketDate;
	@JsonInclude(Include.NON_NULL)
	protected List<IssueTrackerTransaction> transactions;

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public List<IssueTrackerTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<IssueTrackerTransaction> transactions) {
		this.transactions = transactions;
	}

}
