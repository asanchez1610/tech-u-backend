package bbva.techu.edu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class IssueTrackerTransaction {

	@JsonInclude(Include.NON_NULL)
	protected String id;
	@JsonInclude(Include.NON_NULL)
	protected String status;
	@JsonInclude(Include.NON_NULL)
	protected String code;
	@JsonInclude(Include.NON_NULL)
	protected String message;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
