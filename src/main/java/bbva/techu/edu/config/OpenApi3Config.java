package bbva.techu.edu.config;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;

@Configuration
@OpenAPIDefinition(
		info = @Info(
				title = "Backend TechU", 
				version = "1.0"), 
		security = @SecurityRequirement(name = "JWT"), 
		servers = {
		        @Server(url = "https://back-strong-develop.com/", description = "API's"),
		        @Server(url = "http://localhost:8080/", description = "Local API's")
		})

@SecurityScheme(
		name = "JWT", 
		description = "JWT authentication with bearer token", 
		type = SecuritySchemeType.HTTP, 
		scheme = "bearer", 
		bearerFormat = "Bearer [token]")
public class OpenApi3Config {

}