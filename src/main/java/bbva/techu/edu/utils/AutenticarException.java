package bbva.techu.edu.utils;

import org.springframework.http.HttpStatus;

public class AutenticarException extends Exception {

	private static final long serialVersionUID = 1L;
	public HttpStatus codeError;

	public AutenticarException(String message) {
		super(message);
	}

	public AutenticarException(String message, Throwable cause) {
		super(message, cause);
	}

	public AutenticarException(String message, Throwable cause, HttpStatus codeError) {
		super(message, cause);
		this.codeError = codeError;
	}

	public AutenticarException(String message, HttpStatus codeError) {
		super(message);
		this.codeError = codeError;
	}

	public HttpStatus getCodeError() {
		return codeError;
	}

	public void setCodeError(HttpStatus codeError) {
		this.codeError = codeError;
	}

}
