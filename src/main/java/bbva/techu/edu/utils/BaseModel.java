package bbva.techu.edu.utils;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class BaseModel {

	@JsonInclude(Include.NON_NULL)
	protected Date createAt;

	@JsonInclude(Include.NON_NULL)
	protected Date updadateAt;

	@JsonInclude(Include.NON_NULL)
	protected String state;

	@JsonInclude(Include.NON_NULL)
	protected String userCreate;

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Date getUpdadateAt() {
		return updadateAt;
	}

	public void setUpdadateAt(Date updadateAt) {
		this.updadateAt = updadateAt;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUserCreate() {
		return userCreate;
	}

	public void setUserCreate(String userCreate) {
		this.userCreate = userCreate;
	}

}
