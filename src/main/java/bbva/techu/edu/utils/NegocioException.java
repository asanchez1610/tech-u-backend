package bbva.techu.edu.utils;

import org.springframework.http.HttpStatus;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;
	public HttpStatus codeError;

	public NegocioException(String message) {
		super(message);
	}

	public NegocioException(String message, Throwable cause) {
		super(message, cause);
	}

	public NegocioException(String message, Throwable cause, HttpStatus codeError) {
		super(message, cause);
		this.codeError = codeError;
	}

	public NegocioException(String message, HttpStatus codeError) {
		super(message);
		this.codeError = codeError;
	}

	public HttpStatus getCodeError() {
		return codeError;
	}

	public void setCodeError(HttpStatus codeError) {
		this.codeError = codeError;
	}

}
