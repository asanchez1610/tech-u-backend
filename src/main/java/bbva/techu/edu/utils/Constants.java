package bbva.techu.edu.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Constants {

	public static class Security {
		public static final String HEADER = "Authorization";
		public static final String PREFIX = "Bearer ";
		public static final String SECRET = "mySecretKey";
		public static final String ROL_ANY = "ROL_ANY";
		public static final String SOFT_TOKEN_ID = "softtekJWT";
		public static final String ADMIN_NAME = "admin";
	}

	public static class States {
		public static final String ACTIVE = "A";
		public static final String INACTIVE = "I";
	}

	public static Date momentoAhora() {
		Locale locale = new Locale("es", "pe");
		Calendar cal = GregorianCalendar.getInstance(locale);
		return cal.getTime();
	}
	
	public static Date momentoHoyInicio() {
		Locale locale = new Locale("es", "pe");
		Calendar cal = GregorianCalendar.getInstance(locale);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	public static Date momentoHoyFin() {
		Locale locale = new Locale("es", "pe");
		Calendar cal = GregorianCalendar.getInstance(locale);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}
	
	public enum EstadoArqueoCaja {
		REGISTRADO("Registrado"),
		ASIGNADO("Supervisor Asignado"),
		SUPERVISADO("Supervisado");
		
		private String nombre;

		public String getNombre() {
			return nombre;
		}

		EstadoArqueoCaja(String nombre) {
			this.nombre = nombre;
		}
	}

}
