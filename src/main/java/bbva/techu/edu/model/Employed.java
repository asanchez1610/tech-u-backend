package bbva.techu.edu.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bbva.techu.edu.utils.BaseModel;

@Document(collection = "employeds")
public class Employed extends BaseModel {

	@Id
	@JsonInclude(Include.NON_NULL)
	private String id;

	@JsonInclude(Include.NON_NULL)
	private String password;

	@JsonInclude(Include.NON_NULL)
	private String email;

	@JsonInclude(Include.NON_NULL)
	private String registro;

	@JsonInclude(Include.NON_NULL)
	private String nombres;

	@JsonInclude(Include.NON_NULL)
	private String apellidos;

	@JsonInclude(Include.NON_NULL)
	private Value cargo;

	@JsonInclude(Include.NON_NULL)
	private Value oficina;

	@JsonInclude(Include.NON_NULL)
	private String codigoCosto;

	@JsonInclude(Include.NON_NULL)
	private String sexo;

	@JsonInclude(Include.NON_NULL)
	private String urlImgProfile;

	@JsonInclude(Include.NON_NULL)
	private Value rol;

	@Transient
	@JsonInclude(Include.NON_NULL)
	private String token;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegistro() {
		return registro;
	}

	public void setRegistro(String registro) {
		this.registro = registro;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Value getCargo() {
		return cargo;
	}

	public void setCargo(Value cargo) {
		this.cargo = cargo;
	}

	public Value getOficina() {
		return oficina;
	}

	public void setOficina(Value oficina) {
		this.oficina = oficina;
	}

	public String getCodigoCosto() {
		return codigoCosto;
	}

	public void setCodigoCosto(String codigoCosto) {
		this.codigoCosto = codigoCosto;
	}

	public Value getRol() {
		return rol;
	}

	public void setRol(Value rol) {
		this.rol = rol;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getUrlImgProfile() {
		return urlImgProfile;
	}

	public void setUrlImgProfile(String urlImgProfile) {
		this.urlImgProfile = urlImgProfile;
	}

	@Override
	public String toString() {
		return "Employed [id=" + id + ", password=" + password + ", email=" + email + ", registro=" + registro
				+ ", nombres=" + nombres + ", apellidos=" + apellidos + ", cargo=" + cargo + ", oficina=" + oficina
				+ ", codigoCosto=" + codigoCosto + ", sexo=" + sexo + ", urlImgProfile=" + urlImgProfile + ", rol="
				+ rol + ", token=" + token + ", createAt=" + createAt + ", updadateAt=" + updadateAt + ", state="
				+ state + ", userCreate=" + userCreate + "]";
	}

}
