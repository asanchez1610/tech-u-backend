package bbva.techu.edu.model;

import bbva.techu.edu.utils.BaseModel;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "controlDual")
public class ControlDual extends BaseModel {

    @Id
    private String id;
    private Date fechaControl;
    private Employed empleado;
	private String estado;
    private Value Oficina;
    private List<ArqueoCaja> agrupaciones;

    public List<ArqueoCaja> getAgrupaciones() {
		return agrupaciones;
	}

	public void setAgrupaciones(List<ArqueoCaja> agrupaciones) {
		this.agrupaciones = agrupaciones;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getFechaControl() {
        return fechaControl;
    }

    public void setFechaControl(Date fechaControl) {
        this.fechaControl = fechaControl;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Value getOficina() {
        return Oficina;
    }

    public void setOficina(Value oficina) {
        Oficina = oficina;
    }
    public Employed getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Employed empleado) {
		this.empleado = empleado;
	}
}
