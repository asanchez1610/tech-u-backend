package bbva.techu.edu.model;

import bbva.techu.edu.utils.BaseModel;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "arqueoCaja")
public class ArqueoCaja extends BaseModel {
	
	@Id
	private String id;
	private Date fechaSolicitud;
    private Date fechaAprobacion;
    private Float saldoRevisadoSoles;
    private Float saldoRevisadoDolares;
    private Float saldoRevisadoEuros;
    private Float saldoSolesBalancin;
    private Float saldoDolaresBalancin;
    private Float saldoEurosBalancin;
	private Float sobranteSoles;
    private Float sobranteDolares;
    private Float sobranteEuros;
    private Float faltanteSoles;
    private Float faltanteDolares;
    private Float faltanteEuros;
    private Employed solicitante;
    private Employed supervisor;
    private String nroCaja;
    private Value oficina;
    private String estado;

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public Date getFechaAprobacion() {
        return fechaAprobacion;
    }

    public void setFechaAprobacion(Date fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    public Float getSaldoRevisadoSoles() {
        return saldoRevisadoSoles;
    }

    public void setSaldoRevisadoSoles(Float saldoRevisadoSoles) {
        this.saldoRevisadoSoles = saldoRevisadoSoles;
    }

    public Float getSaldoRevisadoDolares() {
        return saldoRevisadoDolares;
    }

    public void setSaldoRevisadoDolares(Float saldoRevisadoDolares) {
        this.saldoRevisadoDolares = saldoRevisadoDolares;
    }

    public Float getSaldoRevisadoEuros() {
        return saldoRevisadoEuros;
    }

    public void setSaldoRevisadoEuros(Float saldoRevisadoEuros) {
        this.saldoRevisadoEuros = saldoRevisadoEuros;
    }

    public Float getSaldoSolesBalancin() {
        return saldoSolesBalancin;
    }

    public void setSaldoSolesBalancin(Float saldoSolesBalancin) {
        this.saldoSolesBalancin = saldoSolesBalancin;
    }

    public Float getSaldoDolaresBalancin() {
        return saldoDolaresBalancin;
    }

    public void setSaldoDolaresBalancin(Float saldoDolaresBalancin) {
        this.saldoDolaresBalancin = saldoDolaresBalancin;
    }

    public Float getSaldoEurosBalancin() {
        return saldoEurosBalancin;
    }

    public void setSaldoEurosBalancin(Float saldoEurosBalancin) {
        this.saldoEurosBalancin = saldoEurosBalancin;
    }

    public Employed getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(Employed solicitante) {
        this.solicitante = solicitante;
    }

    public Employed getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Employed supervisor) {
        this.supervisor = supervisor;
    }

    public String getNroCaja() {
        return nroCaja;
    }

    public void setNroCaja(String nroCaja) {
        this.nroCaja = nroCaja;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Value getOficina() {
        return oficina;
    }

    public void setOficina(Value oficina) {
        this.oficina = oficina;
    }
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
    public Float getSobranteSoles() {
		return sobranteSoles;
	}

	public void setSobranteSoles(Float sobranteSoles) {
		this.sobranteSoles = sobranteSoles;
	}

	public Float getSobranteDolares() {
		return sobranteDolares;
	}

	public void setSobranteDolares(Float sobranteDolares) {
		this.sobranteDolares = sobranteDolares;
	}

	public Float getSobranteEuros() {
		return sobranteEuros;
	}

	public void setSobranteEuros(Float sobranteEuros) {
		this.sobranteEuros = sobranteEuros;
	}

	public Float getFaltanteSoles() {
		return faltanteSoles;
	}

	public void setFaltanteSoles(Float faltanteSoles) {
		this.faltanteSoles = faltanteSoles;
	}

	public Float getFaltanteDolares() {
		return faltanteDolares;
	}

	public void setFaltanteDolares(Float faltanteDolares) {
		this.faltanteDolares = faltanteDolares;
	}

	public Float getFaltanteEuros() {
		return faltanteEuros;
	}

	public void setFaltanteEuros(Float faltanteEuros) {
		this.faltanteEuros = faltanteEuros;
	}
}
