package bbva.techu.edu.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bbva.techu.edu.utils.BaseModel;

@Document(collection = "values")
public class Value extends BaseModel {

	@Id
	@JsonInclude(Include.NON_NULL)
	private String id;

	@JsonInclude(Include.NON_NULL)
	private String name;

	@JsonInclude(Include.NON_NULL)
	private GroupValues group;

	@JsonInclude(Include.NON_NULL)
	private String code;

	@JsonInclude(Include.NON_NULL)
	private String value;

	@JsonInclude(Include.NON_NULL)
	private String valueText;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GroupValues getGroup() {
		return group;
	}

	public void setGroup(GroupValues group) {
		this.group = group;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValueText() {
		return valueText;
	}

	public void setValueText(String valueText) {
		this.valueText = valueText;
	}

	@Override
	public String toString() {
		return "Values [id=" + id + ", name=" + name + ", group=" + group + ", code=" + code + ", value=" + value
				+ ", valueText=" + valueText + ", createAt=" + createAt + ", updadateAt=" + updadateAt + ", state="
				+ state + ", userCreate=" + userCreate + "]";
	}

}
