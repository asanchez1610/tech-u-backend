package bbva.techu.edu.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bbva.techu.edu.utils.BaseModel;

@Document(collection = "group_values")
public class GroupValues extends BaseModel {

	@Id
	@JsonInclude(Include.NON_NULL)
	private String id;

	@JsonInclude(Include.NON_NULL)
	private String name;

	@JsonInclude(Include.NON_NULL)
	private String description;

	@JsonInclude(Include.NON_NULL)
	private String code;
		
	public GroupValues() {
		super();
	}

	public GroupValues(String id, String state) {
		super();
		this.state = state;
		this.id = id;
	}

	public GroupValues(String name, String code, String state, String userCreate) {
		super();
		this.name = name;
		this.code = code;
		this.state = state;
		this.userCreate = userCreate;
		this.createAt = new Date();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
