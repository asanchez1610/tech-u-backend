package bbva.techu.edu.service;

import bbva.techu.edu.model.Employed;
import bbva.techu.edu.utils.AutenticarException;

public interface SecurityService {

	Employed auth(Employed usuario) throws AutenticarException;
	
	String generateToken(Employed usuario);
	
}
