package bbva.techu.edu.service;

import bbva.techu.edu.exception.ControlDualSinAsignacionesException;
import bbva.techu.edu.model.ControlDual;
import bbva.techu.edu.model.Employed;

public interface ControlDualService {

	ControlDual procesar(Employed subGerente) throws ControlDualSinAsignacionesException;

}