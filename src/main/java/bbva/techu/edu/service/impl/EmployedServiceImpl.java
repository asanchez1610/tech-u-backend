package bbva.techu.edu.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import bbva.techu.edu.model.Employed;
import bbva.techu.edu.repository.EmployedRepository;
import bbva.techu.edu.security.utils.JWTUtil;
import bbva.techu.edu.service.EmployedService;
import bbva.techu.edu.utils.Constants;
import bbva.techu.edu.utils.NegocioException;

@Service
public class EmployedServiceImpl implements EmployedService {

	@Autowired
	EmployedRepository employedRepository;

	@Override
	public List<Employed> findEmployedsAll() {
		return this.employedRepository.findByStateIgnoreCaseAndRegistroNotOrderByCreateAtDesc(Constants.States.ACTIVE,
				Constants.Security.ADMIN_NAME);
	}

	@Override
	public Employed saveEmployed(Employed employed) throws NegocioException {
		if(employed.getId() == null) {
			Employed verifyExist = this.findEmployedByRegistro(employed.getRegistro());
			if(verifyExist != null) {
				throw new NegocioException("El usuario con el registro "+ employed.getRegistro() +" ya existe", HttpStatus.BAD_REQUEST);
			}
			employed.setPassword(JWTUtil.hashPassword(employed.getPassword()));
			return this.employedRepository.save(employed);
		}
		return this.employedRepository.save(employed);
	}

	@Override
	public Employed removeEmployed(Employed employed) {
		employed.setState((Constants.States.INACTIVE));
		return this.employedRepository.save(employed);
	}

	@Override
	public Optional<Employed> getEmployed(String id) {
		return this.employedRepository.findById(id);
	}

	@Override
	public Employed findEmployedByRegistro(String registro) {
		return this.employedRepository.findByRegistroIgnoreCase(registro);
	}

}
