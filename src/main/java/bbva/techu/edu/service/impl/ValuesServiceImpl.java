package bbva.techu.edu.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import bbva.techu.edu.model.GroupValues;
import bbva.techu.edu.model.Value;
import bbva.techu.edu.repository.GroupValuesRepository;
import bbva.techu.edu.repository.ValueRepository;
import bbva.techu.edu.service.ValuesService;
import bbva.techu.edu.utils.Constants;

@Service
public class ValuesServiceImpl implements ValuesService {

	@Autowired
	GroupValuesRepository groupValuesRepository;

	@Autowired
	ValueRepository valuesRepository;

	@Override
	public List<GroupValues> findGroupValuesAllActive() {
		return this.groupValuesRepository.findByStateIgnoreCaseOrderByCreateAtDesc(Constants.States.ACTIVE);
	}

	@Override
	public GroupValues saveGroupValue(GroupValues groupValues) {
		return this.groupValuesRepository.save(groupValues);
	}

	@Override
	public GroupValues removeGroupValue(GroupValues groupValues) {
		return this.groupValuesRepository.save(groupValues);
	}

	@Override
	public Optional<GroupValues> getGroupValue(String id) {
		return this.groupValuesRepository.findById(id);
	}

	@Override
	public List<Value> findValuesByGroup(String groupCode) {
		return this.valuesRepository.findByGroupCodeIgnoreCaseAndStateIgnoreCaseOrderByCreateAtDesc(groupCode,
				Constants.States.ACTIVE);
	}

	@Override
	public Value saveValue(Value value) {
		return this.valuesRepository.save(value);
	}

	@Override
	public Value removeValue(Value values) {
		values.setState(Constants.States.INACTIVE);
		return this.valuesRepository.save(values);
	}

	@Override
	public Optional<Value> getValue(String id) {
		return this.valuesRepository.findById(id);
	}

}
