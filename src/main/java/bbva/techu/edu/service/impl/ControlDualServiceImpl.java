package bbva.techu.edu.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bbva.techu.edu.exception.ControlDualSinAsignacionesException;
import bbva.techu.edu.model.ArqueoCaja;
import bbva.techu.edu.model.ControlDual;
import bbva.techu.edu.model.Employed;
import bbva.techu.edu.model.Value;
import bbva.techu.edu.repository.ControlDualRepository;
import bbva.techu.edu.service.ArqueoCajaService;
import bbva.techu.edu.service.ControlDualService;
import bbva.techu.edu.utils.Constants;

@Service
@Transactional
public class ControlDualServiceImpl implements ControlDualService {
	
	@Autowired
    private ArqueoCajaService arqueoCajaService;
	
	@Autowired
    private ControlDualRepository controlDualRepository;
	
	@Override
	public ControlDual procesar(Employed subGerente) throws ControlDualSinAsignacionesException {
		
		Value oficinaSubGerente = subGerente.getOficina();
		List<ArqueoCaja> arqueoCajasDeOficina = arqueoCajaService.listArqueosPendientesOficina(oficinaSubGerente);

		if(arqueoCajasDeOficina.size() ==0) {
			throw new ControlDualSinAsignacionesException();
		}
		
		ControlDual controlDual = new ControlDual();
		controlDual.setEstado("OK");
		controlDual.setEmpleado(subGerente);
		controlDual.setFechaControl(Constants.momentoAhora());
		controlDual.setOficina(oficinaSubGerente);
		
		if(arqueoCajasDeOficina.size() == 1) {
			arqueoCajasDeOficina.get(0).setSupervisor(subGerente);
			arqueoCajasDeOficina.get(0).setEstado(Constants.EstadoArqueoCaja.ASIGNADO.getNombre());
		} else if(arqueoCajasDeOficina.size() == 2) {
			arqueoCajasDeOficina.get(0).setSupervisor(arqueoCajasDeOficina.get(1).getSolicitante());
			arqueoCajasDeOficina.get(0).setEstado(Constants.EstadoArqueoCaja.ASIGNADO.getNombre());
			arqueoCajasDeOficina.get(1).setSupervisor(arqueoCajasDeOficina.get(0).getSolicitante());		
			arqueoCajasDeOficina.get(1).setEstado(Constants.EstadoArqueoCaja.ASIGNADO.getNombre());
		} else {
			int numeroCajaMenor = 1;
			int numeroCajaMayor = arqueoCajasDeOficina.size();
			List<Integer> exclude = new ArrayList<Integer>();
			//arqueoCajasDeOficina //    0,1,2
			List<Integer> newOrder = //  2,0,1
			IntStream.range(0, arqueoCajasDeOficina.size()-2)
			.map(i -> {
				
				List<Integer> excludePropio = new ArrayList<Integer>(exclude);
				excludePropio.add(i+1);
				int nuevoOrden = generateRandom(numeroCajaMenor, numeroCajaMayor, excludePropio);
				exclude.add(nuevoOrden);
				return nuevoOrden;
			})
			.mapToObj(i -> Integer.valueOf(i))
			.collect(Collectors.toList());
			
			List<Integer> newOrderExclude = //tamaño 2
			IntStream.range(1, arqueoCajasDeOficina.size()+1)
					.filter(i -> !newOrder.contains(i) )
					.mapToObj(i -> Integer.valueOf(i))
					.collect(Collectors.toList());	
			
			Collections.sort(newOrderExclude);
			
			if(newOrder.size()+1 == newOrderExclude.get(0) || newOrder.size()+2 == newOrderExclude.get(1)) {
				newOrder.add(newOrderExclude.get(1));
				newOrder.add(newOrderExclude.get(0));
			} else {
				newOrder.add(newOrderExclude.get(0));
				newOrder.add(newOrderExclude.get(1));
			}			
			
			for (int i = 0; i < arqueoCajasDeOficina.size(); i++) {
				ArqueoCaja arqueoCaja = arqueoCajasDeOficina.get(i);
				ArqueoCaja arqueoCajaSupervisor = arqueoCajasDeOficina.get(newOrder.get(i)-1);
				arqueoCaja.setSupervisor(arqueoCajaSupervisor.getSolicitante());
				arqueoCaja.setEstado(Constants.EstadoArqueoCaja.ASIGNADO.getNombre());
			}
		}
		
		controlDual.setAgrupaciones(arqueoCajaService.saveArqueosCaja(arqueoCajasDeOficina));
		
		ControlDual controlDualGuardado = controlDualRepository.insert(controlDual);
		
		return controlDualGuardado;
	}
	
	private int generateRandom(int start, int end, List<Integer> exclude) {
	    Random rand = new Random();
	    int range = end - start +1 - exclude.size();
	    int random = rand.nextInt(range) + 1;

	    for(int i = 0; i < exclude.size(); i++) {
	    	if(!exclude.contains(random))
	    	    return random;
	      random++;
	    }

	    return random;
	}

}
