package bbva.techu.edu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import bbva.techu.edu.model.Employed;
import bbva.techu.edu.repository.EmployedRepository;
import bbva.techu.edu.security.TokenProvider;
import bbva.techu.edu.security.utils.JWTUtil;
import bbva.techu.edu.service.SecurityService;
import bbva.techu.edu.utils.AutenticarException;

@Service
public class SecurityServiceImpl implements SecurityService {

	@Autowired
	private EmployedRepository usuarioRepository;

	@Autowired
	private TokenProvider tokenProvider;

	@Override
	public Employed auth(Employed usuario) throws AutenticarException {
		if (usuario == null) {
			throw new AutenticarException("Los datos del usuario no han sido informados", HttpStatus.BAD_REQUEST);
		}
		if (usuario.getRegistro() == null) {
			throw new AutenticarException("El registro del usuario no ha sido informado", HttpStatus.BAD_REQUEST);
		}
		if (usuario.getPassword() == null) {
			throw new AutenticarException("El password del usuario no ha sido informado", HttpStatus.BAD_REQUEST);
		}
		Employed usuarioAuth = this.usuarioRepository.findByRegistroIgnoreCase(usuario.getRegistro());
		if (usuarioAuth == null) {
			throw new AutenticarException("EL usuario " + usuario.getRegistro() + " no existe.", HttpStatus.BAD_REQUEST);
		}

		if (!JWTUtil.checkPassword(usuario.getPassword(), usuarioAuth.getPassword())) {
			throw new AutenticarException("La contraseña es incorrecta.", HttpStatus.BAD_REQUEST);
		}

		String token = this.tokenProvider.getJWTToken(usuarioAuth.getRegistro(), usuarioAuth);
		usuarioAuth.setToken(token);
		usuarioAuth.setPassword(null);
		return usuarioAuth;

	}

	@Override
	public String generateToken(Employed usuario) {
		// TODO Auto-generated method stub
		return null;
	}

}
