package bbva.techu.edu.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import bbva.techu.edu.model.Value;
import bbva.techu.edu.repository.EmployedRepository;
import org.springframework.beans.BeanUtils;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bbva.techu.edu.exception.ArqueoCajaExistenPendientesException;
import bbva.techu.edu.exception.ArqueoCajaAsignadoOtroSupervisorException;
import bbva.techu.edu.exception.ArqueoCajaNoEncontradaException;
import bbva.techu.edu.exception.ArqueoCajaNoExisteASupervisarException;
import bbva.techu.edu.exception.ArqueoCajaEstadoErroneoException;
import bbva.techu.edu.model.ArqueoCaja;
import bbva.techu.edu.model.Employed;
import bbva.techu.edu.model.Value;
import bbva.techu.edu.repository.ArqueoCajaRepository;
import bbva.techu.edu.service.ArqueoCajaService;
import bbva.techu.edu.utils.Constants;

@Service("ArqueoCajaService")
@Transactional
public class ArqueoCajaServiceImpl implements ArqueoCajaService {

	List<ArqueoCaja> arqueoCajas = new ArrayList<>();
	@Autowired
	private ArqueoCajaRepository arqueoCajaRepository;


	@Override
	public List<ArqueoCaja> listMisArqueos(Employed empleado, Date fechaInicio, Date fechaFin) {
		
		
		List<ArqueoCaja> result = arqueoCajaRepository.findBySolicitanteAndFechaSolicitudBetween(empleado, fechaInicio, fechaFin, Sort.by(Sort.Direction.DESC, "fechaSolicitud"));
		
		return result;
	}

	@Override
	public List<ArqueoCaja> listMisArqueosSupervisados(Employed empleado, Date fechaInicio, Date fechaFin) {
		
		List<ArqueoCaja> result = arqueoCajaRepository.findBySupervisorAndFechaSolicitudBetween(empleado, fechaInicio, fechaFin, Sort.by(Sort.Direction.DESC, "fechaSolicitud"));
		
		return result;
	}

	@Override
	public ArqueoCaja saveArqueoCaja(ArqueoCaja arqueoCaja, Employed empleado) throws ArqueoCajaExistenPendientesException {
		
		
		boolean resultRegistrado = arqueoCajaRepository.existsBySolicitanteAndEstado(empleado, Constants.EstadoArqueoCaja.REGISTRADO.getNombre());
		boolean resultAsignado = arqueoCajaRepository.existsBySolicitanteAndEstado(empleado, Constants.EstadoArqueoCaja.ASIGNADO.getNombre());
		
		if ((resultRegistrado || resultAsignado) && !empleado.getRegistro().equalsIgnoreCase("admin")) {
			throw new ArqueoCajaExistenPendientesException();
		}

	 //Obtener Saldos Sobrantes y Faltantes
	    Float saldoSoles = arqueoCaja.getSaldoSolesBalancin() - arqueoCaja.getSaldoRevisadoSoles() ;
	    Float saldoDolares = arqueoCaja.getSaldoDolaresBalancin() - arqueoCaja.getSaldoRevisadoDolares() ;
	    Float saldoEuros = arqueoCaja.getSaldoEurosBalancin() - arqueoCaja.getSaldoRevisadoEuros() ;
	    Float sobranteSoles = 0.0f;
	    Float sobranteDolares = 0.0f;
	    Float sobranteEuros = 0.0f;
        Float faltanteSoles = 0.0f;
        Float faltanteDolares = 0.0f;
        Float faltanteEuros = 0.0f;

	    if(saldoSoles < 0.0) {
	     sobranteSoles = Math.abs(saldoSoles);
	    } else {
	      faltanteSoles = saldoSoles;
	    }

	    if(saldoDolares < 0.0) {
          sobranteDolares = Math.abs(saldoDolares);
        } else {
          faltanteDolares = saldoDolares;
        }

        if(saldoEuros < 0.0) {
          sobranteEuros = Math.abs(saldoEuros);
        } else {
          faltanteEuros = saldoEuros;
        }
        arqueoCaja.setSobranteSoles(sobranteSoles);	    
        arqueoCaja.setSobranteDolares(sobranteDolares);	    
        arqueoCaja.setSobranteEuros(sobranteEuros);
        arqueoCaja.setFaltanteSoles(faltanteSoles);
        arqueoCaja.setFaltanteDolares(faltanteDolares);
        arqueoCaja.setFaltanteEuros(faltanteEuros);
        	    
       //Obtener datos del empleado
       arqueoCaja.setSolicitante(empleado);

       //Obtener oficina
       arqueoCaja.setOficina(empleado.getOficina());
       
       //Obtener fecha registro
      arqueoCaja.setFechaSolicitud(Constants.momentoAhora());
       
       //Setear estado
       arqueoCaja.setEstado(Constants.EstadoArqueoCaja.REGISTRADO.getNombre());


	    return arqueoCajaRepository.save(arqueoCaja);
	}
	
	@Override
	public ArqueoCaja modifyArqueoCaja(String id, ArqueoCaja arqueoCaja, Employed supervisor) throws ArqueoCajaNoEncontradaException, ArqueoCajaEstadoErroneoException, ArqueoCajaAsignadoOtroSupervisorException {
		
		Optional<ArqueoCaja> arqueoCajaDataOp = arqueoCajaRepository.findById(id);
		
		//no existe la caja
		if(arqueoCajaDataOp.isEmpty()) {
			throw new ArqueoCajaNoEncontradaException();
		}

		ArqueoCaja arqueoCajaData = arqueoCajaDataOp.get();
		
		// si arqueCaja ya esta Supervisada
		if(!Constants.EstadoArqueoCaja.ASIGNADO.getNombre().equals(arqueoCajaData.getEstado())) {
			throw new ArqueoCajaEstadoErroneoException();
		}
		
		// si el supervisor es quien debe dar la aprobacion
		if(!supervisor.getId().equals(arqueoCajaData.getSupervisor().getId())) {
			throw new ArqueoCajaAsignadoOtroSupervisorException();
		}
		
		arqueoCajaData.setFechaAprobacion(Constants.momentoAhora());
		arqueoCajaData.setEstado(Constants.EstadoArqueoCaja.SUPERVISADO.getNombre());
	
	    return arqueoCajaRepository.save(arqueoCajaData);
	}

	/*@Override
	public ArqueoCaja generaSupervisor(String fecha, Employed subGerente) {
		Optional<Employed> subGerenteOp = employedRepository.findById(subGerente.getId());
		if( subGerenteOp.get())
		if(arqueoCajaDataOp.isEmpty()) {
			throw new RuntimeException("Id de ArqueoCaja no encontrada.");
		}


		return arqueoCajaRepository.save(arqueoCajaSupervisor);
	}*/

	@Override
	public List<ArqueoCaja> generarConsolidadoOficina(Date fechaInicio, Date fechaFin, Employed empleado) {
		List<ArqueoCaja> arqueoCajasDeOficina = arqueoCajaRepository.findByOficinaAndFechaSolicitudBetween(empleado.getOficina(),fechaInicio,fechaFin, Sort.by(Sort.Direction.DESC,"fechaSolicitud"));

		return arqueoCajasDeOficina;
	}

	@Override
	public List<ArqueoCaja> listArqueosPendientesOficina(Value oficina) {
		Date hoyInicio = Constants.momentoHoyInicio();
		Date hoyFin = Constants.momentoHoyFin();
		return arqueoCajaRepository.findByOficinaAndEstadoAndFechaSolicitudBetween(oficina, Constants.EstadoArqueoCaja.REGISTRADO.getNombre(), hoyInicio, hoyFin);
	}

	@Override
	public List<ArqueoCaja> saveArqueosCaja(List<ArqueoCaja> arqueoCajas) {
		return arqueoCajaRepository.saveAll(arqueoCajas);
	}

	@Override
	public ArqueoCaja getArqueoCajaSupervisar(Employed empleado) throws ArqueoCajaNoExisteASupervisarException {
		List<ArqueoCaja> result = arqueoCajaRepository.findBySupervisorAndEstado(empleado, Constants.EstadoArqueoCaja.ASIGNADO.getNombre());
		if(result.isEmpty()) {
			throw new ArqueoCajaNoExisteASupervisarException();
		}
		return result.get(0);
	}
	
}
