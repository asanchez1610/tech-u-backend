package bbva.techu.edu.service;

import java.util.Date;
import java.util.List;

import bbva.techu.edu.exception.ArqueoCajaExistenPendientesException;
import bbva.techu.edu.exception.ArqueoCajaAsignadoOtroSupervisorException;
import bbva.techu.edu.exception.ArqueoCajaNoEncontradaException;
import bbva.techu.edu.exception.ArqueoCajaNoExisteASupervisarException;
import bbva.techu.edu.exception.ArqueoCajaEstadoErroneoException;
import bbva.techu.edu.model.ArqueoCaja;
import bbva.techu.edu.model.Employed;
import bbva.techu.edu.model.Value;

public interface ArqueoCajaService {
	
	List<ArqueoCaja> listMisArqueos(Employed empleado, Date fechaInicio, Date fechaFin);
	
	List<ArqueoCaja> listMisArqueosSupervisados(Employed empleado, Date fechaInicio, Date fechaFin);

	public ArqueoCaja saveArqueoCaja(ArqueoCaja arqueoCaja, Employed empleado) throws ArqueoCajaExistenPendientesException;

	public ArqueoCaja modifyArqueoCaja(String id, ArqueoCaja arqueoCaja, Employed supervisor) throws ArqueoCajaNoEncontradaException, ArqueoCajaEstadoErroneoException, ArqueoCajaAsignadoOtroSupervisorException;

	List<ArqueoCaja> generarConsolidadoOficina(Date fechaInicio, Date fechaFin, Employed empleado);

	List<ArqueoCaja> listArqueosPendientesOficina(Value oficinaSubGerente);

	List<ArqueoCaja> saveArqueosCaja(List<ArqueoCaja> arqueoCajasDeOficina);
	
	ArqueoCaja getArqueoCajaSupervisar(Employed empleado) throws ArqueoCajaNoExisteASupervisarException;

}
