package bbva.techu.edu.service;

import java.util.List;
import java.util.Optional;

import bbva.techu.edu.model.Employed;
import bbva.techu.edu.utils.NegocioException;

public interface EmployedService {

	List<Employed> findEmployedsAll();

	Employed saveEmployed(Employed employed) throws NegocioException;

	Employed removeEmployed(Employed employed);

	Optional<Employed> getEmployed(String id);
	
	Employed findEmployedByRegistro(String registro);

}
