package bbva.techu.edu.service;

import java.util.List;
import java.util.Optional;

import bbva.techu.edu.model.GroupValues;
import bbva.techu.edu.model.Value;

public interface ValuesService {
	
	List<GroupValues> findGroupValuesAllActive();
	
	GroupValues saveGroupValue(GroupValues groupValues);
	
	GroupValues removeGroupValue(GroupValues groupValues);
	
	Optional<GroupValues> getGroupValue(String id);
	
	List<Value> findValuesByGroup(String groupCode);
	
	Value saveValue(Value value);
	
	Value removeValue(Value values);
	
	Optional<Value> getValue(String id);

}
